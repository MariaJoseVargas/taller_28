import pandas as pd 
import matplotlib.pyplot as pl
import numpy as np

miDataframe=pd.read_csv('Valor producto.csv')
print(miDataframe)

print ("La suma del valor de todos los productos es: ", miDataframe["VALOR"].sum())
print("El producto con el menor valor es: ",miDataframe["VALOR"].min())

miDataframe.plot()
pl.show()

for i in range(5):
    print("La raíz cúbica del producto ",i+1,": ",np.cbrt(miDataframe["VALOR"][i]))